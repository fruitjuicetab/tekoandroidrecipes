package com.example.recipes.model

data class Favorites(
val id: Int,
val name: String
)
