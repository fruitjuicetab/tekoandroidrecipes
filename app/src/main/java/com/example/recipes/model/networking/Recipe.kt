package com.example.recipes.model.networking

import com.google.gson.annotations.SerializedName

data class Recipe(
        @SerializedName("title")
        val title: String = ""
    )
