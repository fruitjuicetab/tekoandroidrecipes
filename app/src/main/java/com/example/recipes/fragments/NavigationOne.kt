package com.example.recipes.fragments

import android.content.Context
import  android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.recipes.R
import com.example.recipes.adapter.RecipesAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.recipes.networking.MyOwnRetrofitClient
import kotlinx.android.synthetic.main.fragment_list_fragments.*
import retrofit2.Call
import retrofit2.Callback
import  retrofit2.Response
import android.util.Log
import android.widget.ToggleButton
import com.example.recipes.model.networking.Recipe


class NavigationOne : Fragment() {
    var adapter = RecipesAdapter()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_navigation_one, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //Inform the recycler view that it uses the adapter we created.
        recycler_view.adapter = adapter

        //The layout manager will tell the recycler view HOW to render the rows.
        recycler_view.layoutManager = LinearLayoutManager(view.context)

        //Create the Retrofit Client
        val client = MyOwnRetrofitClient()
        adapter.onItemClick = { recipe ->

            // do something with your item
            Log.d("TAG", recipe.title)
        }

        client.apiClient.listRecipes()
            .enqueue(object : Callback<List<Recipe>> {
                override fun onFailure(call: Call<List<Recipe>>, t: Throwable) {
                    Log.d("API Calling", "API Call Failing")
                }

                override fun onResponse(
                    call: Call<List<Recipe>>,
                    response: Response<List<Recipe>>
                ) {
                    if (response.isSuccessful) {
                        //Set the data
                        adapter.setData((response.body()!!.toList()))

                        //updating the startup state
                        writeStartupStatus()

                        Log.d("Answer", "success")
                    } else {
                        //TODO something went wrong check what is is.
                    }

                }
            })
    }


    private fun writeStartupStatus() {

        //persistent storage
        view!!.context
            .getSharedPreferences("myPreferences", Context.MODE_PRIVATE)
            .edit()
            .putString("isStartup", "no")
            //apply storage async, commit storage in thread
            .apply()
    }


}















