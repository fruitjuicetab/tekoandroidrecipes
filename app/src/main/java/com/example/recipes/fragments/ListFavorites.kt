package com.example.recipes.fragments
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.recipes.R
import com.example.recipes.adapter.FavoritesAdapter
import com.example.recipes.model.Favorites
import kotlinx.android.synthetic.main.fragment_list_fragments.*


class ListFavorites : Fragment() {
    //Create an instance of the adapter that we want to use
    var adapter = FavoritesAdapter()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_navigation_one, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //Create a dummy list of data
        val data: MutableList<Favorites> = arrayListOf()
        for (i in 0..3) {
            data.add(Favorites(1, "Apfel-Käse-Kuchen"))
            data.add(Favorites(2, "Birchermüesli"))
            data.add(Favorites(3, "Blumenkohlwings"))
        }

        //Inform the recycler view that it uses the adapter we created.
        recycler_view.adapter = adapter
        //The layout manager will tell the recycler view HOW to render the rows.
        recycler_view.layoutManager = LinearLayoutManager(view.context)
        //Set the data
        adapter.setData(data)
    }
}