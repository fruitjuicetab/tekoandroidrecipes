package com.example.recipes.networking

import com.example.recipes.model.networking.Recipe
import retrofit2.Call
import retrofit2.http.GET



/**
 * This interface will represent our API.
 */
interface MeServiceInterface {

    @GET("/index.json")
    fun listRecipes(): Call<List<Recipe>>
}




