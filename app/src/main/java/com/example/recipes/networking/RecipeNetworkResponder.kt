package com.example.recipes.networking

import com.example.recipes.model.networking.Recipe
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import android.util.Log



class RecipeNetworkResponder : Callback<List<Recipe>> {

    override fun onFailure(call: Call<List<Recipe>>, t: Throwable) {
        Log.d("answer", "There is a problem")
    }

    override fun onResponse(call: Call<List<Recipe>>, response: Response<List<Recipe>>) {
        Log.d("answer", response.body().toString())
    }
}