package com.example.recipes.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ToggleButton
import androidx.recyclerview.widget.RecyclerView
import com.example.recipes.R
import com.example.recipes.model.Favorites
import com.example.recipes.model.networking.Recipe
import kotlinx.android.synthetic.main.item_favorites.view.*

class FavoritesAdapter: RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var data: List<Favorites> = arrayListOf()

    /**
     * Creates the placeholder for every data item that will be used.
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        //Get the class responsible for creating a code representation of the xml file.
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.item_favorites, parent, false)
        //Now create the placeholder object with our layout
        return FavoritesViewHolder(view)
    }

    /**
     * Return the size of data items that we currently want to display.
     */
    override fun getItemCount(): Int {
        return data.size
    }

    /**
     * Call the function for binding the data and the layout
     */
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is FavoritesViewHolder) {
            holder.bind(data[position])
        }
    }

    /**
     * Set the new data set to this adapter
     */
    fun setData(newData: List<Favorites>) {
        data = newData
        notifyDataSetChanged()
    }

    /**
     * Create our own class for binding the data with the row layout
     */
    class FavoritesViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(favorites: Favorites) {
            itemView.name.text = "${favorites.name} "
        }
    }


}