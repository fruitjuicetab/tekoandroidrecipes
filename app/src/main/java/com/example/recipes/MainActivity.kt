package com.example.recipes

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import kotlinx.android.synthetic.main.activity_main.*




class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)



        //Bind navigation component with tab bar
        val navController = findNavController(R.id.nav_host_fragment)
        bottom_bar.setupWithNavController(navController)



/*
       /*
        var scaleAnimation = ScaleAnimation(0.7f, 1.0f, 0.7f, 1.0f, Animation.RELATIVE_TO_SELF, 0.7f, Animation.RELATIVE_TO_SELF, 0.7f)

        scaleAnimation.duration = 500
        var bounceInterpolator = BounceInterpolator()
        scaleAnimation.interpolator =bounceInterpolator
        */
        button_favorite.setOnCheckedChangeListener(object:View.OnClickListener, CompoundButton.OnCheckedChangeListener {
            override fun onCheckedChanged(p0: CompoundButton?, p1: Boolean) {
                /*
                p0?.startAnimation(scaleAnimation);

                Log.d("fav", "am i here") //To change body of created functions use File | Settings | File Templates.
            */
            }
            override fun onClick(p0: View?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }
        })
*/
    }
}




